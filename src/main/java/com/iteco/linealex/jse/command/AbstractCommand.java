package com.iteco.linealex.jse.command;

import com.iteco.linealex.jse.api.ITerminalService;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.enumerate.Role;

import java.util.Arrays;
import java.util.List;

public abstract class AbstractCommand {

    protected Bootstrap serviceLocator;

    protected ITerminalService terminalService;

    protected List<Role> roles = Arrays.asList(Role.ADMINISTRATOR, Role.ORDINARY_USER);

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;

    public abstract boolean secure();

    public List<Role> getAvailableRoles() {
        return roles;
    }

    public Bootstrap getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(Bootstrap serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public ITerminalService getTerminalService() {
        return terminalService;
    }

    public void setScanner(ITerminalService terminalService) {
        this.terminalService = terminalService;
    }

}
