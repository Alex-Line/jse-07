package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import com.iteco.linealex.jse.exception.ShortPasswordException;


public class UserCreateCommand extends UserAbstractCommand {

    @Override
    public String command() {
        return "user-create";
    }

    @Override
    public String description() {
        return "REGISTER NEW USER IF THEY DO NOT EXIST. (AVAILABLE FOR AMDMINS ONLY)";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER USER LOGIN. IT MUST BE UNIQUE!");
        final String login = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER USER PASSWORD. IT MUST BE 8 DIGITS OR LONGER");
        final String password = serviceLocator.getTerminalService().nextLine();
        if (password.length() < 8) throw new ShortPasswordException();
        final String hashPassword = TransformatorToHashMD5.getHash(password);
        User newUser = new User(login, hashPassword);
        newUser = serviceLocator.getUserService().createUser(newUser,
                serviceLocator.getSelectedEntityService().getSelectedUser());
        System.out.println("[WAS REGISTERED NEW USER]");
        System.out.println(newUser);
        System.out.println("[OK]\n");
    }

}
