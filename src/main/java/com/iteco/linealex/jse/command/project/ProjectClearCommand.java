package com.iteco.linealex.jse.command.project;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;

import java.util.Collection;

public class ProjectClearCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "CLEAR THE LIST OF PROJECTS";
    }

    @Override
    public void execute() throws Exception {
        final User user = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (user.getRole() == Role.ADMINISTRATOR) {
            final Collection<Project> collection = serviceLocator.getProjectService().getAllEntities();
            if (collection == null) return;
            System.out.println("[All PROJECTS REMOVED]\n");
            return;
        }
        final Collection<Project> userCollection = serviceLocator.getProjectService().removeAllEntities(user.getId());
        if (userCollection == null) {
            System.out.println("[THERE IS NOT ANY " + user.getName() + "'s PROJECTS FOR REMOVING]\n");
            return;
        }
        serviceLocator.getSelectedEntityService().setSelectedProject(null);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
