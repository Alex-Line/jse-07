package com.iteco.linealex.jse.command.task;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.util.DateFormatter;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "CREATE A NEW TASK IN PROJECT OR WITHOUT IT";
    }

    @Override
    public void execute() throws Exception {
        final User user = serviceLocator.getSelectedEntityService().getSelectedUser();
        final Project project = serviceLocator.getSelectedEntityService().getSelectedProject();
        final Task task = new Task();
        task.setUserId(user.getId());
        System.out.println("[ENTER TASK NAME]");
        final String taskName = serviceLocator.getTerminalService().nextLine();
        task.setName(taskName);
        System.out.println("[ENTER TASK DESCRIPTION]");
        final String taskDescription = serviceLocator.getTerminalService().nextLine();
        task.setDescription(taskDescription);
        System.out.println("[ENTER TASK START DATE IN FORMAT: DD.MM.YYYY]");
        task.setDateStart(DateFormatter.formatStringToDate(serviceLocator.getTerminalService().nextLine()));
        System.out.println("[ENTER TASK FINISH DATE IN FORMAT: DD.MM.YYYY]");
        task.setDateFinish(DateFormatter.formatStringToDate(serviceLocator.getTerminalService().nextLine()));
        if (project == null) task.setProjectId(null);
        else task.setProjectId(project.getId());
        final Task insertedTask = serviceLocator.getTaskService().persist(task);
        if (insertedTask == null) {
            System.out.println("[THERE IS SOME NULL OR EMPTY DATA]\n");
            return;
        }
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
