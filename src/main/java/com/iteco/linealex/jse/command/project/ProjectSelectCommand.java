package com.iteco.linealex.jse.command.project;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.User;

public class ProjectSelectCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-select";
    }

    @Override
    public String description() {
        return "SELECT A PROJECT FOR MANIPULATION FURTHER";
    }

    @Override
    public void execute() throws Exception {
        final User user = serviceLocator.getSelectedEntityService().getSelectedUser();
        System.out.println("[ENTER THE NAME OF PROJECT FOR SELECTION]");
        final String projectName = terminalService.nextLine();
        final Project selectedProject = serviceLocator.getProjectService().selectEntity(projectName, user.getId());
        if (selectedProject == null) {

            System.out.println("[THERE IS NOT SUCH PROJECT AS " + projectName
                    + ". PLEASE TRY TO SELECT AGAIN]\n");
            return;
        }
        serviceLocator.getSelectedEntityService().setSelectedProject(selectedProject);
        System.out.println("[WAS SELECTED]");
        System.out.println(selectedProject);
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
