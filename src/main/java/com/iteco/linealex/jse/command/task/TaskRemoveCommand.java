package com.iteco.linealex.jse.command.task;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;

public class TaskRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "REMOVE ONE TASK IN SELECTED PROJECT OR WITHOUT IT";
    }

    @Override
    public void execute() throws Exception {
        final User user = serviceLocator.getSelectedEntityService().getSelectedUser();
        System.out.println("[ENTER TASK NAME]");
        final String taskName = terminalService.nextLine();
        System.out.println("REMOVING TASK...");
        Task task = null;
        if (serviceLocator.getSelectedEntityService().getSelectedProject() != null) {
            task = serviceLocator.getTaskService().removeEntity(
                    serviceLocator.getSelectedEntityService().getSelectedProject().getId(),
                    taskName, user.getId());
        } else task = serviceLocator.getTaskService().removeEntity(taskName, user.getId());
        if (task == null) {
            System.out.println("[THERE IS NOT SUCH TASK AS " + taskName
                    + ". PLEASE TRY AGAIN OR SELECT PROJECT FIRST]\n");
            return;
        }
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
