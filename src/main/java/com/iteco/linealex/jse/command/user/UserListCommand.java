package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.exception.LowAccessLevelException;

import java.util.Collection;

public class UserListCommand extends UserAbstractCommand {

    @Override
    public String command() {
        return "user-list";
    }

    @Override
    public String description() {
        return "SHOW ALL USERS REGISTERED IN THE TASK MANAGER. (AVAILABLE FOR AMDMINS ONLY)";
    }

    @Override
    public void execute() throws Exception {
        final User selectedUser = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (selectedUser.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        final Collection<User> collection = serviceLocator.getUserService().getUsers(selectedUser);
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY USERS YET]\n");
            return;
        }
        int index = 1;
        for (User user : collection) {
            System.out.println(index + ". " + user);
            index++;
        }
        System.out.println();
    }

}
