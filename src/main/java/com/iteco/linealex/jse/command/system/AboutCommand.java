package com.iteco.linealex.jse.command.system;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.jcabi.manifests.Manifests;

public class AboutCommand extends AbstractCommand {
    @Override
    public String command() {
        return "about";
    }

    @Override
    public String description() {
        return "PRINT BUILD NUMBER";
    }

    @Override
    public void execute() throws Exception {
        final String buildNumber = Manifests.read("buildNumber");
        final String developer = Manifests.read("developer");
        System.out.println("build number: " + buildNumber + "\n" + "developer: " + developer);
    }

    @Override
    public boolean secure() {
        return false;
    }
}

