package com.iteco.linealex.jse.command.system;

import com.iteco.linealex.jse.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

public class HelpCommand extends AbstractCommand {

    private List<String> help = new ArrayList<>();

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "SHOW ALL AVAILABLE COMMANDS";
    }

    @Override
    public void execute() throws Exception {
        for (AbstractCommand command : serviceLocator.getCommands()) {
            System.out.println(command.command() + " : " + command.description());
        }
    }

    @Override
    public boolean secure() {
        return false;
    }

}
