package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.enumerate.Role;

import java.util.Arrays;
import java.util.List;

public abstract class UserAbstractCommand extends AbstractCommand {

    List<Role> roles = Arrays.asList(Role.ADMINISTRATOR);

    @Override
    public List<Role> getAvailableRoles() {
        return roles;
    }

    @Override
    public boolean secure() {
        return true;
    }

}
