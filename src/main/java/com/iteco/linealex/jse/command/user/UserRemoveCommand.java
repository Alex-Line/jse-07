package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import com.iteco.linealex.jse.exception.LowAccessLevelException;
import com.iteco.linealex.jse.exception.UserIsNotExistException;
import com.iteco.linealex.jse.exception.UserIsNotLogInException;
import com.iteco.linealex.jse.exception.WrongPasswordException;

public class UserRemoveCommand extends UserAbstractCommand {

    @Override
    public String command() {
        return "user-remove";
    }

    @Override
    public String description() {
        return "REMOVING THE USER BY LOGIN";
    }

    @Override
    public void execute() throws Exception {
        final User selectedUser = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (selectedUser == null) throw new UserIsNotLogInException();
        System.out.println("ENTER USER LOGIN");
        final String login = serviceLocator.getTerminalService().nextLine();
        if(serviceLocator.getUserService().getUser(login, selectedUser) == null)
            throw new UserIsNotExistException();
        if (selectedUser.getRole() != Role.ADMINISTRATOR || selectedUser.getName().equals(login))
            throw new LowAccessLevelException();
        System.out.println("ENTER USER PASSWORD");
        final String password = serviceLocator.getTerminalService().nextLine();
        final String hashPassword = TransformatorToHashMD5.getHash(password);
        if (hashPassword.equals(selectedUser.getHashPassword())) {
            serviceLocator.getUserService().removeEntity(login);
            serviceLocator.getProjectService().removeAllEntities(selectedUser.getId());
            serviceLocator.getTaskService().removeAllTasks(selectedUser.getId());
            System.out.println("[USER " + selectedUser.getName() + " REMOVED]");
            System.out.println("[ALSO WAS REMOVED ALL IT'S PROJECTS AND TASKS]");
            System.out.println("[OK]\n");
        } else throw new WrongPasswordException();
    }

}
