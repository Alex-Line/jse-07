package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.entity.User;

public class UserShowCommand extends UserAbstractCommand {

    @Override
    public String command() {
        return "user-show";
    }

    @Override
    public String description() {
        return "SHOW ANY USER BY LOGIN. (AVAILABLE FOR ADMINS ONLY)";
    }

    @Override
    public void execute() throws Exception {
        final User selectedUser = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (selectedUser == null) {
            System.out.println("[YOU MUST BE AUTHORISED TO DO THAT]\n");
            return;
        }
        System.out.println("ENTER USER LOGIN");
        final String login = terminalService.nextLine();
        if (login.equals(selectedUser.getName())) {
            System.out.println(selectedUser);
            return;
        }
        final User user = serviceLocator.getUserService().getUser(login, selectedUser);
        System.out.println(user);
        System.out.println();
    }

}
