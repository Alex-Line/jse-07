package com.iteco.linealex.jse.command.system;

import com.iteco.linealex.jse.command.AbstractCommand;

public class ClearSelectionCommand extends AbstractCommand {

    @Override
    public String command() {
        return "selection-clear";
    }

    @Override
    public String description() {
        return "RESET SELECTION TASK AND PROJECT";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getSelectedEntityService().setSelectedProject(null);
        serviceLocator.getSelectedEntityService().setSelectedTask(null);
        System.out.println("[ALL SELECTION WAS CANCELED]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}
