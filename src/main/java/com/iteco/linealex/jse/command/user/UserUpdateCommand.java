package com.iteco.linealex.jse.command.user;

public class UserUpdateCommand extends UserAbstractCommand {

    @Override
    public String command() {
        return "user-update";
    }

    @Override
    public String description() {
        return "UPDATE USER'S PASSWORD";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER OLD PASSWORD");
        final String oldPassword = terminalService.nextLine();
        System.out.println("ENTER NEW PASSWORD");
        final String newPassword = terminalService.nextLine();
        System.out.println("ENTER NEW PASSWORD AGAIN");
        if (newPassword.equals(terminalService.nextLine())) {
            serviceLocator.getUserService().updateUserPassword(oldPassword, newPassword,
                    serviceLocator.getSelectedEntityService().getSelectedUser());
            System.out.println("[OK]\n");
        } else System.out.println("MISTAKE IN THE REPEATING OF NEW PASSWORD\n");
    }

}
