package com.iteco.linealex.jse.command.project;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;

import java.util.Collection;

public class ProjectListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String description() {
        return "SHOW ALL PROJECTS";
    }

    @Override
    public void execute() throws Exception {
        final User user = serviceLocator.getSelectedEntityService().getSelectedUser();
        System.out.println("[PROJECT LIST]");
        Collection<Project> collection = null;
        if (user.getRole() == Role.ADMINISTRATOR) {
            collection = serviceLocator.getProjectService().getAllEntities();
        } else collection = serviceLocator.getProjectService().getAllEntities(user.getId());
        if (collection == null || collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY PROJECTS]\n");
            return;
        }
        int index = 1;
        for (Project project : collection) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}
