package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.User;

public class UserLogInCommand extends AbstractCommand {

    @Override
    public String command() {
        return "login";
    }

    @Override
    public String description() {
        return "SIGN IN INTO YOUR ACCOUNT";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER USER LOGIN");
        final String login = terminalService.nextLine();
        System.out.println("ENTER USER PASSWORD");
        final String password = terminalService.nextLine();
        final User user = serviceLocator.getUserService().signInUser(login, password,
                serviceLocator.getSelectedEntityService().getSelectedUser());
        if (user == null) System.out.println("[THERE WAS LOGIN OTHER USER. TRY AGAIN]\n");
        else System.out.println("[USER " + user.getName() + " ENTERED TO TASK MANAGER]\n");
        serviceLocator.getSelectedEntityService().setSelectedUser(user);
    }

    @Override
    public boolean secure() {
        return false;
    }

}
