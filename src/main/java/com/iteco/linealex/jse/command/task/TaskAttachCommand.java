package com.iteco.linealex.jse.command.task;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.User;

public class TaskAttachCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-attach";
    }

    @Override
    public String description() {
        return "ATTACH THE SELECTED TASK TO PROJECT";
    }

    @Override
    public void execute() throws Exception {
        final User user = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (serviceLocator.getSelectedEntityService().getSelectedTask() == null) {
            System.out.println("[YOU DID NOT SELECT ANY TASK! SELECT ANY AND TRY AGAIN]\n");
            return;
        }
        System.out.println("[ENTER PROJECT NAME]");
        final String projectName = terminalService.nextLine();
        final Project project = serviceLocator.getProjectService().getEntityByName(projectName, user.getId());
        boolean result = serviceLocator.getTaskService().attachTaskToProject(project.getId(),
                serviceLocator.getSelectedEntityService().getSelectedTask());
        if (result) System.out.println("[OK]\n");
        else System.out.println("THERE IS NOT SUCH PROJECT! PLEASE TRY AGAIN!\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
