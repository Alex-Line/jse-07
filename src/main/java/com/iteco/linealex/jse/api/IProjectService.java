package com.iteco.linealex.jse.api;

import com.iteco.linealex.jse.entity.Project;

import java.util.Collection;

public interface IProjectService<Project> extends IService<Project> {

    public Collection<Project> getAllEntities(final String userId);

    public Project removeEntity(final String entityName, final String userId);

    public Project getEntityByName(final String entityName, final String userId);

    public Collection<Project> removeAllEntities(final String userId);

}
