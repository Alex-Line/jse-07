package com.iteco.linealex.jse.api;

import com.iteco.linealex.jse.exception.*;

import java.util.Collection;

public interface IUserService<User> extends IService<User> {

    public User signInUser(final String login,
                           final String password,
                           final User selectedUser) throws Exception;

    public User createUser(final User user, final User selectedUser) throws TaskManagerException;

    public User updateUserPassword(final String oldPassword,
                                   final String newPassword,
                                   final User selectedUser) throws Exception;

    public User getUser(final String login, final User selectedUser) throws TaskManagerException;

    public Collection<User> getUsers(final User selectedUser) throws LowAccessLevelException;

}
