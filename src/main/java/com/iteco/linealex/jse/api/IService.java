package com.iteco.linealex.jse.api;

import com.iteco.linealex.jse.exception.InsetExistingEntityException;

import java.util.Collection;

public interface IService<T> {

    public T selectEntity(final String entityName, final String userId);

    public T persist(final T entity) throws InsetExistingEntityException;

    public T merge(final T entity);

    public Collection<T> getAllEntities();

    public T removeEntity(final String entityName);

    public Collection<T> removeAllEntities();

}
