package com.iteco.linealex.jse.api;

public interface IEntity {

    public String getId();

    public String getName();

    public void setName(final String name);

}
