package com.iteco.linealex.jse.api;

import com.iteco.linealex.jse.exception.InsetExistingEntityException;

import java.util.Collection;

public interface IRepository<T> {

    public boolean contains(final String entityName);

    public boolean contains(final String entityName, final String userId);

    public Collection<T> findAll();

    public T findOne(final String name);

    public T persist(final T example) throws InsetExistingEntityException;

    public T merge(final T example) throws InsetExistingEntityException;

    public T remove(final String name);

    public Collection<T> removeAll();

}