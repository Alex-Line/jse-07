package com.iteco.linealex.jse.api;

import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;

public interface ISelectedEntityService {

    public Project getSelectedProject();

    public Task getSelectedTask();

    public User getSelectedUser();

    public void setSelectedProject(Project selectedProject);

    public void setSelectedTask(Task selectedTask);

    public void setSelectedUser(User selectedUser);

}
