package com.iteco.linealex.jse.api;

import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;


public interface ServiceLocator {

    IProjectService<Project> getProjectService();

    ITaskService<Task> getTaskService();

    IUserService<User> getUserService();

    ITerminalService getTerminalService();

    ISelectedEntityService getSelectedEntityService();

}
