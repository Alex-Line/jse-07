package com.iteco.linealex.jse.api;

public interface ITerminalService {

    public String nextLine();

}
