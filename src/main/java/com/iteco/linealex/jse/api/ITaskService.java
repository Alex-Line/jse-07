package com.iteco.linealex.jse.api;

import com.iteco.linealex.jse.entity.Task;

import java.util.Collection;

public interface ITaskService<Task> extends IService<Task> {

    Collection<Task> removeAllTasksFromProject(final String projectName, final String id);

    public Task selectEntity(final String projectId, final String taskName, final String userId);

    public Task selectEntity(final String taskName, final String userId);

    public Collection<Task> getAllEntities(final String userId);

    public Collection<Task> getAllEntities(final String projectId, final String userId);

    public Task removeEntity(final String entityName, final String userId);

    public Task removeEntity(final String projectId, final String entityName, final String userId);

    public Collection<Task> removeAllTasks(final String userId);

    public boolean attachTaskToProject(final String projectId, final Task selectedEntity);

}
