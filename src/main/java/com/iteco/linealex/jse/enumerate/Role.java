package com.iteco.linealex.jse.enumerate;

public enum Role {

    ADMINISTRATOR("administrator"),
    ORDINARY_USER("ordinary user");

    private String name;

    Role(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
