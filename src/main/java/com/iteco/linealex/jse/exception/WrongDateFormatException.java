package com.iteco.linealex.jse.exception;

public class WrongDateFormatException extends TaskManagerException {

    @Override
    public String getMessage() {
        return "[WRONG DATE FORMAT! COMMAND WAS INTERRUPTED]\n";
    }

}
