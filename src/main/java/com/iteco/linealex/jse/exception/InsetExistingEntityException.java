package com.iteco.linealex.jse.exception;

public class InsetExistingEntityException extends TaskManagerException {

    @Override
    public String getMessage() {
        return "THERE IS SUCH ELEMENT ALREADY. COMMAND WAS INTERRUPTED\n";
    }
}
