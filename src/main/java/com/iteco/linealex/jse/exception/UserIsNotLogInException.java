package com.iteco.linealex.jse.exception;

public class UserIsNotLogInException extends TaskManagerException {

    @Override
    public String getMessage() {
        return "THERE IS NOT ANY ACTIVE USER. PLEASE LOGIN AND TRY AGAIN. COMMAND WAS INTERRUPTED\n";
    }

}
