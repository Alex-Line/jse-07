package com.iteco.linealex.jse.exception;

public class TaskManagerException extends Exception {

    @Override
    public String getMessage() {
        return "[UNDEFINED TASK MANAGER EXCEPTION. COMMAND WAS INTERRUPTED]\n";
    }
}
