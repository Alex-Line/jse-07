package com.iteco.linealex.jse.exception;

public class ShortPasswordException extends TaskManagerException {

    @Override
    public String getMessage() {
        return "ENTERED PASSWORD IS TO SHORT. COMMAND WAS INTERRUPTED\n";
    }

}
