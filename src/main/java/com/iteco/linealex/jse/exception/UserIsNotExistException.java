package com.iteco.linealex.jse.exception;

public class UserIsNotExistException extends TaskManagerException {

    @Override
    public String getMessage() {
        return "USER IS NOT EXIST. COMMAND WAS INTERRUPTED\n";
    }
}
