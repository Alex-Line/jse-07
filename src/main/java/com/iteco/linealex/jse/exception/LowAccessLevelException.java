package com.iteco.linealex.jse.exception;

public class LowAccessLevelException extends TaskManagerException {

    @Override
    public String getMessage() {
        return "YOU DO NOT HAVE RIGHTS TO DO THAT. PLEASE CONTACT ADMINISTRATOR FOR MORE INFORMATION. COMMAND WAS INTERRUPTED\n";
    }
}
