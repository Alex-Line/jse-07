package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.ISelectedEntityService;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;

public class SelectedEntityService implements ISelectedEntityService {

    private Project selectedProject;

    private Task selectedTask;

    private User selectedUser;

    @Override
    public Project getSelectedProject() {
        return selectedProject;
    }

    @Override
    public Task getSelectedTask() {
        return selectedTask;
    }

    @Override
    public User getSelectedUser() {
        return selectedUser;
    }

    @Override
    public void setSelectedProject(Project selectedProject) {
        this.selectedProject = selectedProject;
    }

    @Override
    public void setSelectedTask(Task selectedTask) {
        this.selectedTask = selectedTask;
    }

    @Override
    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }

}
