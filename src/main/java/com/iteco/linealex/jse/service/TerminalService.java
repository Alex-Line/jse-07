package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.ITerminalService;

import java.util.Scanner;

public final class TerminalService implements ITerminalService {

    private final Scanner scanner = new Scanner(System.in);

    @Override
    public String nextLine() {
        return scanner.nextLine().trim();
    }

}
