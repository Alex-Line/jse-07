package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.IProjectService;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.repository.AbstractRepository;
import com.iteco.linealex.jse.exception.InsetExistingEntityException;

import java.util.Collection;

public class ProjectService extends AbstractService<Project> implements IProjectService<Project> {

    public ProjectService(final AbstractRepository<Project> repository) {
        super(repository);
    }

    @Override
    public Project persist(final Project project) throws InsetExistingEntityException {
        if (project == null) return null;
        if (project.getName() == null || project.getName().isEmpty()) return null;
        if (project.getDescription() == null || project.getDescription().isEmpty()) return null;
        if (project.getUserId() == null || project.getUserId().isEmpty()) return null;
        return repository.persist(project);
    }

    @Override
    public Project merge(final Project entity) {
        final Project existedProject = repository.findOne(entity.getName(), entity.getUserId());
        if (existedProject == null) return null;
        existedProject.setDescription(entity.getDescription());
        existedProject.setDateStart(entity.getDateStart());
        existedProject.setDateFinish(entity.getDateFinish());
        return existedProject;
    }

    @Override
    public Collection<Project> getAllEntities(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return repository.findAll(userId);
    }

    @Override
    public Project removeEntity(final String projectName, final String userId) {
        if (projectName == null || projectName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        final Project project = repository.remove(projectName, userId);
        return project;
    }

    @Override
    public Collection<Project> removeAllEntities(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return repository.removeAll(userId);
    }

    @Override
    public Project getEntityByName(final String projectName, final String userId) {
        if (projectName == null || projectName.isEmpty()) return null;
        return repository.findOne(projectName, userId);
    }

    @Override
    public Project selectEntity(final String entityName, final String userId) {
        if (entityName == null || entityName.isEmpty()) return null;
        return getEntityByName(entityName, userId);
    }

}
