package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.IUserService;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.repository.AbstractRepository;
import com.iteco.linealex.jse.util.*;
import com.iteco.linealex.jse.exception.*;

import java.util.Collection;


public class UserService extends AbstractService<User> implements IUserService<User> {

    public UserService(final AbstractRepository<User> repository) {
        super(repository);
    }

    public User signInUser(final String login, final String password,
                           final User selectedUser) throws Exception {
        if (selectedUser != null) return null;
        if (login == null || login.isEmpty()) throw new LoginIncorrectException();
        if (!repository.contains(login)) throw new UserIsNotExistException();
        if (password == null || password.isEmpty()) throw new WrongPasswordException();
        final String hashPassword = TransformatorToHashMD5.getHash(password);
        final User user = repository.findOne(login);
        if (!user.getHashPassword().equals(hashPassword)) throw new WrongPasswordException();
        return user;
    }

    @Override
    public User createUser(final User user, final User selectedUser) throws TaskManagerException {
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        if (user == null) return null;
        if (repository.contains(user.getName())) throw new InsetExistingEntityException();
        return persist(user);
    }

    @Override
    public User selectEntity(final String entityName, final String userId) {
        if (entityName == null || entityName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        final User user = repository.findOne(entityName, userId);
        return user;
    }

    @Override
    public User persist(final User entity) throws InsetExistingEntityException {
        if (entity == null) return null;
        if (entity.getName() == null || entity.getName().isEmpty()) return null;
        return repository.persist(entity);
    }

    @Override
    public User merge(final User entity) {
        final User user = repository.findOne(entity.getName());
        if (user == null) return null;
        user.setName(entity.getName());
        user.setRole(entity.getRole());
        user.setHashPassword(entity.getHashPassword());
        return user;
    }

    @Override
    public User removeEntity(final String entityName, final String userId) {
        if (entityName == null || entityName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.remove(entityName, userId);
    }

    public User updateUserPassword(final String oldPassword,
                                   final String newPassword,
                                   final User selectedUser) throws Exception {
        if (selectedUser == null) throw new UserIsNotLogInException();
        if (oldPassword == null || oldPassword.isEmpty()) throw new WrongPasswordException();
        final String hashOldPassword = TransformatorToHashMD5.getHash(oldPassword);
        if (!selectedUser.getHashPassword().equals(hashOldPassword)) throw new WrongPasswordException();
        if (newPassword == null || newPassword.isEmpty()) throw new WrongPasswordException();
        if (newPassword.length() < 8) throw new ShortPasswordException();
        final String hashNewPassword = TransformatorToHashMD5.getHash(newPassword);
        selectedUser.setHashPassword(hashNewPassword);
        return selectedUser;
    }

    public User getUser(final String login, final User selectedUser) throws TaskManagerException {
        if (selectedUser == null) throw new UserIsNotLogInException();
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        if (login == null || login.isEmpty()) throw new LoginIncorrectException();
        final User user = repository.findOne(login);
        if (user == null) throw new UserIsNotExistException();
        return user;
    }

    public Collection<User> getUsers(final User selectedUser) throws LowAccessLevelException {
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        return repository.findAll();
    }

}
