package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.ITaskService;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.repository.AbstractRepository;
import com.iteco.linealex.jse.exception.InsetExistingEntityException;

import java.util.Collection;
import java.util.LinkedList;

public class TaskService extends AbstractService<Task> implements ITaskService<Task> {

    public TaskService(final AbstractRepository<Task> repository) {
        super(repository);
    }

    public Task selectEntity(final String projectId, final String taskName, final String userId) {
        if (projectId == null || projectId.isEmpty()) return selectEntity(taskName, userId);
        if (taskName == null || taskName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.findOne(taskName, projectId, userId);
    }

    public Task selectEntity(final String taskName, final String userId) {
        if (taskName == null || taskName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.findOne(taskName, userId);
    }

    public Collection<Task> getAllEntities(final String userId) {
        if (userId == null || userId.isEmpty()) return new LinkedList<>();
        return repository.findAll(userId);
    }

    public Collection<Task> getAllEntities(final String projectId, final String userId) {
        if (projectId == null || projectId.isEmpty()) return getAllEntities(userId);
        if (userId == null || userId.isEmpty()) return new LinkedList<>();
        return repository.findAll(projectId, userId);
    }

    @Override
    public Task persist(final Task entity) throws InsetExistingEntityException {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return null;
        if (entity.getUserId() == null || entity.getUserId().isEmpty()) return null;
        return repository.persist(entity);
    }

    @Override
    public Task merge(final Task entity) {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return null;
        if (entity.getUserId() == null || entity.getUserId().isEmpty()) return null;
        return null;
    }

    public Task removeEntity(final String entityName, final String userId) {
        if (entityName == null || entityName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.remove(entityName, userId);
    }

    public Task removeEntity(final String projectId, final String entityName, final String userId) {
        if (projectId == null || projectId.isEmpty()) return removeEntity(entityName, userId);
        if (entityName == null || entityName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.remove(entityName, projectId, userId);
    }

    public Collection<Task> removeAllTasksFromProject(final String projectId, final String userId) {
        if (projectId == null || projectId.isEmpty()) return removeAllTasks(userId);
        if (userId == null || userId.isEmpty()) return null;
        final Collection<Task> collection = repository.removeAll(userId, projectId);
        return collection;
    }

    public Collection<Task> removeAllTasks(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return repository.removeAll(userId);
    }

    public boolean attachTaskToProject(final String projectId, final Task selectedEntity) {
        if (projectId == null || projectId.isEmpty()) return false;
        if (selectedEntity == null) return false;
        for (Task task : repository.findAll()) {
            if (task.getProjectId() != null) continue;
            if (!task.getId().equals(selectedEntity.getId())) continue;
            task.setProjectId(projectId);
            return true;
        }
        return false;
    }

}
