package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.IService;
import com.iteco.linealex.jse.entity.AbstractEntity;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.repository.AbstractRepository;
import com.iteco.linealex.jse.exception.InsetExistingEntityException;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    protected AbstractRepository<T> repository;

    public AbstractService(final AbstractRepository<T> repository) {
        this.repository = repository;
    }

    @Override
    public Collection<T> getAllEntities() {
        return repository.findAll();
    }

    @Override
    public abstract T persist(final T entity) throws InsetExistingEntityException;

    @Override
    public T removeEntity(final String entityName) {
        if (entityName == null || entityName.isEmpty()) return null;
        final T entity = repository.remove(entityName);
        return entity;
    }

    public abstract T removeEntity(final String entityName, final String userId);

    @Override
    public Collection<T> removeAllEntities() {
        return repository.removeAll();
    }

    public Collection<Project> removeAllEntities(final String userId) {
        return new ArrayList<>();
    }

}
