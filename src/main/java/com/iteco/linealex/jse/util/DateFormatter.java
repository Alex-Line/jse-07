package com.iteco.linealex.jse.util;

import com.iteco.linealex.jse.exception.WrongDateFormatException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {

    private static SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

    public static String formatDateToString(Date date) {
        return format.format(date);
    }

    public static Date formatStringToDate(String stringDate) throws WrongDateFormatException {
        try {
            return format.parse(stringDate);
        } catch (ParseException e) {
            throw new WrongDateFormatException();
        }

    }
}
