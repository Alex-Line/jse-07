package com.iteco.linealex.jse.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class TransformatorToHashMD5 {

    public static String getHash(String convertingString) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        StringBuffer hexString = new StringBuffer(convertingString);
        for (int i = 0; i < 5; i++) {
            hexString.append("tzEGMt5k");
            md.update(hexString.toString().getBytes());
            hexString = new StringBuffer();
            for (byte aByteData : md.digest()) {
                String hex = Integer.toHexString(0xff & aByteData);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
        }
        md.update(convertingString.getBytes());

        for (byte aByteData : md.digest()) {
            String hex = Integer.toHexString(0xff & aByteData);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

}
