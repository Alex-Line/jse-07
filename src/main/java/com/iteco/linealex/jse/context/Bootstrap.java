package com.iteco.linealex.jse.context;

import com.iteco.linealex.jse.api.*;
import com.iteco.linealex.jse.command.*;
import com.iteco.linealex.jse.command.project.*;
import com.iteco.linealex.jse.command.system.*;
import com.iteco.linealex.jse.command.task.*;
import com.iteco.linealex.jse.command.user.*;
import com.iteco.linealex.jse.entity.*;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.repository.*;
import com.iteco.linealex.jse.service.*;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import com.iteco.linealex.jse.exception.*;

import java.security.NoSuchAlgorithmException;
import java.util.*;

public final class Bootstrap implements com.iteco.linealex.jse.api.ServiceLocator {

    private final ITerminalService terminalService = new TerminalService();

    private final IProjectService<Project> projectService = new ProjectService(new ProjectRepository());

    private final ITaskService<Task> taskService = new TaskService(new TaskRepository());

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final IUserService<User> userService = new UserService(new UserRepository());

    private final ISelectedEntityService selectedEntityService = new SelectedEntityService();

    {
        registry(new AboutCommand());
        registry(new HelpCommand());
        registry(new ClearSelectionCommand());
        registry(new ExitCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveCommand());
        registry(new ProjectSelectCommand());
        registry(new TaskAttachCommand());
        registry(new TaskClearAllCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskRemoveCommand());
        registry(new TaskSelectCommand());
        registry(new TaskListCommand());
        registry(new UserCreateCommand());
        registry(new UserLogInCommand());
        registry(new UserListCommand());
        registry(new UserShowCommand());
        registry(new UserLogOutCommand());
        registry(new UserUpdateCommand());
        registry(new UserRemoveCommand());
    }

    private void registry(AbstractCommand command) {
        final String commandName = command.command();
        if (commandName == null || commandName.isEmpty()) return;
        final String commandDescription = command.description();
        if (commandDescription == null || commandDescription.isEmpty()) return;
        command.setServiceLocator(this);
        command.setScanner(terminalService);
        commands.put(command.command(), command);
    }

    public void start() {
        createInitialUsers();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = terminalService.nextLine().toLowerCase();
            execute(command);
        }
    }

    private void execute(final String command) {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        try {
            if (abstractCommand.secure() && getSelectedEntityService().getSelectedUser() == null)
                throw new UserIsNotLogInException();
            if (getSelectedEntityService().getSelectedUser() != null &&
                    !abstractCommand.getAvailableRoles().contains(getSelectedEntityService().getSelectedUser().getRole())) {
                System.out.println("[THIS COMMAND AVAILABLE FOR USER ACCESS LEVEL :]");
                System.out.println(abstractCommand.getAvailableRoles());
                System.out.println("[YOUR LEVEL IS :]");
                System.out.println(getSelectedEntityService().getSelectedUser().getRole());
                throw new LowAccessLevelException();
            }
            abstractCommand.execute();
        } catch (TaskManagerException taskException) {
            System.out.println(taskException.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createInitialUsers() {
        try {
            final User admin = new User("admin", TransformatorToHashMD5.getHash("11111111"));
            admin.setRole(Role.ADMINISTRATOR);
            getSelectedEntityService().setSelectedUser(admin);
            userService.createUser(admin, getSelectedEntityService().getSelectedUser());
            getSelectedEntityService().setSelectedUser(admin);
            userService.createUser(new User("startuser", TransformatorToHashMD5.getHash("22222222")),
                    getSelectedEntityService().getSelectedUser());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (TaskManagerException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public IProjectService<Project> getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService<Task> getTaskService() {
        return taskService;
    }

    @Override
    public IUserService<User> getUserService() {
        return userService;
    }

    @Override
    public ITerminalService getTerminalService() {
        return terminalService;
    }

    @Override
    public ISelectedEntityService getSelectedEntityService() {
        return selectedEntityService;
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

}