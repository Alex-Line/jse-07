package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.IRepository;
import com.iteco.linealex.jse.entity.Task;

import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements IRepository<Task> {

    @Override
    public Collection<Task> findAll(final String projectId, final String userId) {
        final Collection<Task> collection = new LinkedList<>();
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null || !entry.getValue().getUserId().equals(userId)) continue;
            if (entry.getValue().getProjectId() != null && entry.getValue().getProjectId().equals(projectId)) {
                collection.add(entry.getValue());
            }
        }
        return collection;
    }

    public Collection<Task> findAll(final String userId) {
        final Collection<Task> collection = new LinkedList<>();
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getProjectId() != null) continue;
            if (entry.getValue().getUserId().equals(userId))
                collection.add(entry.getValue());
        }
        return collection;
    }

    @Override
    public Task findOne(final String taskName, final String userId) {
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getProjectId() != null) continue;
            return entry.getValue();
        }
        return null;
    }

    @Override
    public Task findOne(final String taskName, final String projectId, final String userId) {
        if (projectId == null) return findOne(taskName, userId);
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getProjectId() == null) continue;
            if (entry.getValue().getProjectId().equals(projectId)) continue;
            return entry.getValue();
        }
        return null;
    }

    @Override
    public Task persist(final Task example) {
        boolean contains = true;
        if (example.getProjectId() == null) {
            contains = contains(example.getProjectId(), example.getName());
        } else {
            contains = contains(example.getProjectId(), example.getName(), example.getUserId());
        }
        if (contains) return null;
        entityMap.put(example.getId(), example);
        return example;
    }

    @Override
    public Task merge(final Task example) {
        final Task oldTask = findOne(example.getName(), example.getProjectId(), example.getUserId());
        if (oldTask == null) return null;
        oldTask.setName(example.getName());
        oldTask.setDescription(example.getDescription());
        oldTask.setDateStart(example.getDateStart());
        oldTask.setDateFinish(example.getDateFinish());
        oldTask.setProjectId(example.getProjectId());
        oldTask.setUserId(example.getUserId());
        persist(example);
        return oldTask;
    }

    @Override
    public Task remove(final String taskName) {
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (!(entry.getValue().getProjectId() == null)) continue;
            return entityMap.remove(entry.getKey());
        }
        return null;
    }

    @Override
    public Task remove(final String taskName, final String userId) {
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (!(entry.getValue().getProjectId() == null)) continue;
            return entityMap.remove(entry.getKey());
        }
        return null;
    }

    @Override
    public Task remove(final String taskName, final String projectId, final String userId) {
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (entry.getValue().getProjectId() == null) continue;
            if (!entry.getValue().getProjectId().equals(projectId)) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            return entityMap.remove(entry.getKey());
        }
        return null;
    }

    @Override
    public Collection<Task> removeAll(final String userId) {
        final Collection<Task> collection = new ArrayList<>();
        for (Iterator<Map.Entry<String, Task>> iterator = entityMap.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<String, Task> entry = iterator.next();
            if (entry.getValue().getProjectId() != null) continue;
            if (entry.getValue().getUserId() != null && entry.getValue().getUserId().equals(userId)) {
                collection.add(entry.getValue());
                iterator.remove();
            }
        }
        return collection;
    }

    @Override
    public Collection<Task> removeAll(final String userId, final String projectId) {
        final Collection<Task> collection = new ArrayList<>();
        for (Iterator<Map.Entry<String, Task>> iterator = entityMap.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<String, Task> entry = iterator.next();
            if (entry.getValue().getProjectId() == null ||
                    !(entry.getValue().getProjectId().equals(projectId))) continue;
            if (entry.getValue().getUserId() != null && entry.getValue().getUserId().equals(userId)) {
                collection.add(entry.getValue());
                iterator.remove();
            }
        }
        return collection;
    }

    @Override
    public boolean contains(final String entityName) {
        return false;
    }

    public boolean contains(final String taskName, final String userId) {
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getProjectId() != null) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            return true;
        }
        return false;
    }

    public boolean contains(final String projectId, final String taskName, final String userId) {
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getProjectId() != null &&
                    !entry.getValue().getProjectId().equals(projectId)) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            return true;
        }
        return false;
    }

}
