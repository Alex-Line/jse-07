package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.IRepository;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.exception.InsetExistingEntityException;

import java.util.*;

public class ProjectRepository extends AbstractRepository<Project> implements IRepository<Project> {

    @Override
    public boolean contains(final String entityName) {
        return false;
    }

    public boolean contains(final String name, final String userId) {
        for (Map.Entry<String, Project> entry : entityMap.entrySet()) {
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (!entry.getValue().getName().equals(name)) continue;
            return true;
        }
        return false;
    }

    @Override
    public Collection<Project> findAll(final String userId) {
        final Collection<Project> collection = new LinkedList<>();
        for (Map.Entry<String, Project> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId().equals(userId))
                collection.add(entry.getValue());
        }
        return collection;
    }

    @Override
    public Project findOne(final String name, final String userId) {
        for (Map.Entry<String, Project> entry : entityMap.entrySet()) {
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (entry.getValue().getName().equals(name)) {
                return entry.getValue();
            }
        }
        return null;
    }

    @Override
    public Project persist(final Project example) throws InsetExistingEntityException {
        for (Map.Entry<String, Project> entry : entityMap.entrySet()) {
            if (!entry.getValue().getUserId().equals(example.getUserId())) continue;
            if (entry.getValue().getName().equals(example.getName()))
                throw new InsetExistingEntityException();

        }
        entityMap.put(example.getId(), example);
        return example;
    }

    @Override
    public Project merge(final Project example) {
        Project oldProject = findOne(example.getName(), example.getUserId());
        if (oldProject == null) return null;
        oldProject.setName(example.getName());
        oldProject.setDescription(example.getDescription());
        oldProject.setDateStart(example.getDateStart());
        oldProject.setDateFinish(example.getDateFinish());
        return oldProject;
    }

    @Override
    public Project remove(String name, String userId) {
        Project project = null;
        for (Map.Entry<String, Project> entry : entityMap.entrySet()) {
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (entry.getValue().getName().equals(name)) {
                project = entry.getValue();
                entityMap.remove(project.getId());
            }
        }
        return project;
    }

    @Override
    public Collection<Project> removeAll(String userId) {
        final Collection<Project> collection = new ArrayList<>();
        for (Iterator<Map.Entry<String, Project>> iterator = entityMap.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<String, Project> entry = iterator.next();
            if (entry.getValue().getUserId().equals(userId)) {
                collection.add(entry.getValue());
                iterator.remove();
            }
        }
        return collection;
    }

}
