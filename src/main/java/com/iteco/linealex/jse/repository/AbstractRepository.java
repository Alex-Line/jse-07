package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.IRepository;
import com.iteco.linealex.jse.entity.AbstractEntity;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.exception.InsetExistingEntityException;

import java.util.*;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    protected Map<String, T> entityMap = new LinkedHashMap<>();

    @Override
    public abstract boolean contains(final String entityName, final String userId);

    @Override
    public Collection<T> findAll() {
        final Collection<T> collection = new LinkedList<>(entityMap.values());
        return collection;
    }

    public abstract Collection<T> findAll(final String userId);

    public Collection<T> findAll(final String projectId, final String userId) {
        return new ArrayList<>();
    }

    @Override
    public T findOne(final String entityName) {
        for (Map.Entry<String, T> entry : entityMap.entrySet()) {
            if (entry.getValue().getName().equals(entityName))
                return entry.getValue();
        }
        return null;
    }

    public T findOne(final String entityName, final String userId) {
        return null;
    }

    public T findOne(final String entityName, final String projectId, final String userId) {
        return null;
    }

    @Override
    public T persist(final T example) throws InsetExistingEntityException {
        if (entityMap.containsValue(example)) throw new InsetExistingEntityException();
        return entityMap.put(example.getId(), example);
    }

    @Override
    public T merge(final T example) throws InsetExistingEntityException {
        if (entityMap.containsValue(example)) throw new InsetExistingEntityException();
        return entityMap.put(example.getId(), example);
    }

    @Override
    public T remove(final String entityName) {
        for (Map.Entry<String, T> entry : entityMap.entrySet()) {
            if (!entry.getValue().getName().equals(entityName)) continue;
            return entityMap.remove(entry.getKey());
        }
        return null;
    }

    public T remove(final String entityName, final String userId) {
        return null;
    }

    public T remove(final String name, final String projectId, final String userId) {
        return null;
    }

    @Override
    public Collection<T> removeAll() {
        final Collection<T> collection = new LinkedList<>(entityMap.values());
        entityMap.clear();
        return collection;
    }

    public Collection<T> removeAll(final String userId) {
        return new ArrayList<>();
    }

    public Collection<Task> removeAll(final String userId, final String projectId) {
        return new ArrayList<>();
    }

}
