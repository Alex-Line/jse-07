package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.IRepository;
import com.iteco.linealex.jse.entity.User;

import java.util.*;

public class UserRepository extends AbstractRepository<User> implements IRepository<User> {

    @Override
    public boolean contains(final String entityName, final String userId) {
        for (Map.Entry<String, User> entry : entityMap.entrySet()) {
            if (entry.getValue().getName().equals(entityName)) return true;
        }
        return false;
    }

    public boolean contains(final String login) {
        for (Map.Entry<String, User> entry : entityMap.entrySet()) {
            if (!entry.getValue().getName().equals(login)) continue;
            return true;
        }
        return false;
    }

    @Override
    public Collection<User> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        final Collection<User> collection = new LinkedList<>();
        for (Map.Entry<String, User> entry : entityMap.entrySet()) {
            if (entry.getValue().getId().equals(userId))
                collection.add(entry.getValue());
        }
        return collection;
    }

    @Override
    public User merge(final User example) {
        final User user = findOne(example.getName());
        if (user == null) return null;
        user.setRole(example.getRole());
        user.setHashPassword(example.getHashPassword());
        return user;
    }

}