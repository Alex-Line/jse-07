package com.iteco.linealex.jse.entity;

import com.iteco.linealex.jse.api.IEntity;

import java.util.UUID;

public class AbstractEntity implements IEntity {

    private final String id;

    private String name;

    public AbstractEntity() {
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

}
