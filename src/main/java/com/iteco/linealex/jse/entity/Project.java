package com.iteco.linealex.jse.entity;

import com.iteco.linealex.jse.api.IEntity;
import com.iteco.linealex.jse.util.DateFormatter;

import java.util.Date;
import java.util.UUID;

public class Project extends AbstractEntity implements IEntity {

    private final String id = UUID.randomUUID().toString();

    private String name = "unnamed project";

    private String description = "";

    private Date dateStart = new Date();

    private Date dateFinish = new Date();

    private String userId = null;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Project: " + name +
                " { ID = " + id +
                ",\n    description = '" + description + '\'' +
                ",\n    Start Date = " + DateFormatter.formatDateToString(dateStart) +
                ",\n    Finish Date = " + DateFormatter.formatDateToString(dateFinish) +
                ",\n    User ID = " + userId +
                " }";
    }

}
