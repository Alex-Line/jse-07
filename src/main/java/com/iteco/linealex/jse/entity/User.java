package com.iteco.linealex.jse.entity;

import com.iteco.linealex.jse.api.IEntity;
import com.iteco.linealex.jse.enumerate.Role;

import java.util.UUID;

public class User extends AbstractEntity implements IEntity {

    private String login;

    private String hashPassword;

    private Role role = Role.ORDINARY_USER;

    private final String id = UUID.randomUUID().toString();

    public User(String login, String hashPassword) {
        this.login = login;
        this.hashPassword = hashPassword;
    }

    @Override
    public String getName() {
        return login;
    }

    @Override
    public void setName(String name) {
        this.login = name;
    }

    public String getHashPassword() {
        return hashPassword;
    }

    public void setHashPassword(String hashPassword) {
        this.hashPassword = hashPassword;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "User " + login +
                ",\n     role = " + role.getName() +
                ",\n     id = " + id +
                '}';
    }
}
